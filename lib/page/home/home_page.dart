import 'dart:ui';

import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cuberto_bottom_bar/cuberto_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:gradient_bottom_navigation_bar/gradient_bottom_navigation_bar.dart';
import 'package:provider/provider.dart';
import 'package:state_management_example/model/repo/food/food_repository.dart';
import 'package:state_management_example/model/repo/game/game_repository.dart';
import 'package:state_management_example/page/product/product_list_page.dart';
import 'package:state_management_example/shared/utils/app_variables.dart';
import 'package:state_management_example/store/page/home/home_page_store.dart';
import 'package:state_management_example/util/route/navigation_util.dart';
import 'package:state_management_example/util/route/router.gr.dart';
import 'package:state_management_example/util/style/app_colors.dart';
import 'package:state_management_example/util/widget/cart_app_bar.dart';
import 'package:state_management_example/util/widget/horizontal_listview_container.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    double statusbar = MediaQuery.of(context).padding.top;
    double screenWidth = MediaQuery.of(context).size.width;
    GameRepository gameRepository = Provider.of<GameRepository>(context);
    FoodRepository foodRepository = Provider.of<FoodRepository>(context);
    HomePageStore homePageStore = Provider.of<HomePageStore>(context);
    return Scaffold(
      body: Container(
        color: AppColors.appGray1,
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: statusbar),
              child: ListView(
                padding: const EdgeInsets.only(top: 0.0),
                children: <Widget>[
                  SizedBox(
                    height: 56,
                  ),
                  CarouselSlider.builder(
                    height: MediaQuery.of(context).size.height / 3.5,
                    autoPlay: true,
                    viewportFraction: 1.0,
                    itemCount: 15,
                    itemBuilder: (BuildContext context, int itemIndex) {
                      return Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Colors.pink,
                              Colors.black,
                            ],
                          ),
                        ),
                        width: MediaQuery.of(context).size.width,
                        child: Text(itemIndex.toString()),
                      );
                    },
                  ),
                  HorizontalListViewContainer(
                    productList: gameRepository.getRemoteGameList(),
                    listViewHeaderTitle: 'Games',
                    onTapLoadMore: () async {
                      homePageStore.setVisiblePage(false);
                      homePageStore.setVisiblePage(
                        await NavigationUtil.navigateToProductListPage(
                          Category.Games,
                          () => gameRepository.getRemoteGameList(),
                        ),
                      );
                    },
                  ),
                  HorizontalListViewContainer(
                    productList: foodRepository.getRemoteFoodList(),
                    listViewHeaderTitle: 'Food',
                    onTapLoadMore: () {
                      Router.navigator.pushNamed(
                        Router.productListPageRoute,
                        arguments: ProductListPageArgumentContainer(
                          Category.Food,
                          () => foodRepository.getRemoteFoodList(),
                        ),
                      );
                    },
                  ),
//                  CategoryCard(
//                      title: 'Games',
//                      text: 'Diverse library of games for all platforms.',
//                      color: AppColors.appBlue1,
//                      iconData: Icons.gamepad,
//                      category: Category.Games),
//                  CategoryCard(
//                      title: 'Food',
//                      text: 'We have food for any occasion.',
//                      color: AppColors.appGreen,
//                      iconData: Icons.fastfood,
//                      category: Category.Food),
                  Container(
                    height: 200,
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: ClipRect(
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 25,
                    sigmaY: 25,
                  ),
                  child: Container(
                    height: 56,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(
                            Icons.home,
                            color: Theme.of(context).bottomAppBarColor,
                          ),
                          onPressed: () {},
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.home,
                            color: Theme.of(context).bottomAppBarColor,
                          ),
                          onPressed: () {},
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.home,
                            color: Theme.of(context).bottomAppBarColor,
                          ),
                          onPressed: () {},
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment(-1.0, -1.0),
                        end: Alignment(1.0, 1.0),
                        colors: [
                          AppColors.appBlue1.withOpacity(0.8),
                          AppColors.appGreen.withOpacity(0.69),
                        ],
                      ),
                    ),
//                    child: BottomNavigationBar(
//                      fixedColor: Colors.white,
//                      backgroundColor: Colors.transparent,
//                      currentIndex: 0,
//                      onTap: (index) {},
//                      items: [
//                        BottomNavigationBarItem(
//                          icon: Icon(
//                            Icons.list,
//                          ),
//                          title: Text('Todos'),
//                        ),
//                        BottomNavigationBarItem(
//                          icon: Icon(
//                            Icons.show_chart,
//                          ),
//                          title: Text('Stats'),
//                        ),
//                      ],
//                    ),
                  ),
                ),
              ),
            ),
//            Align(
//              alignment: Alignment.center,
//              child: ClipRect(
//                child: BackdropFilter(
//                  filter: ImageFilter.blur(
//                    sigmaX: 25,
//                    sigmaY: 25,
//                  ),
//                  child: Container(
//                    height: 56,
//                    decoration: BoxDecoration(
//                      gradient: LinearGradient(
//                        begin: Alignment(-1.0, -1.0),
//                        end: Alignment(1.0, 1.0),
//                        colors: [
//                          AppColors.appBlue1.withOpacity(0.8),
//                          AppColors.appGreen.withOpacity(0.69),
//                        ],
//                      ),
//                    ),
//                  ),
//                ),
//              ),
//            ),
//            Align(
//              alignment: Alignment.bottomCenter,
//              child: CubertoBottomBar(
//                inactiveIconColor: Colors.blue,
//                tabStyle: CubertoTabStyle.STYLE_FADED_BACKGROUND, // By default its CubertoTabStyle.STYLE_NORMAL
//                selectedTab: _selectedIndex, // By default its 0, Current page which is fetched when a tab is clickd, should be set here so as the change the tabs, and the same can be done if willing to programmatically change the tab.
//                tabs: [
//                  TabData(
//                    iconData: Icons.home,
//                    title: "Home",
//                    tabColor: Colors.deepPurple,
//                  ),
//                  TabData(
//                    iconData: Icons.search,
//                    title: "Search",
//                    tabColor: Colors.pink,
//                  ),
//                  TabData(
//                      iconData: Icons.access_alarm,
//                      title: "Alarm",
//                      tabColor: Colors.amber),
//                  TabData(
//                      iconData: Icons.settings,
//                      title: "Settings",
//                      tabColor: Colors.teal),
//                ],
//                onTabChangedListener: (position, title, color) {
//                  setState(() {
//                    _selectedIndex = position;
////                    currentTitle = title;
////                    currentColor = color;
//                  });
//                },
//              ),
//            ),
            CartAppBar(
              title: 'Home',
              inHomePage: true,
            ),
//            Align(
//              alignment: Alignment.bottomCenter,
//              child: Row(
//                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                children: <Widget>[
//                  IconButton(
//                    icon: Icon(
//                      Icons.home,
//                      color: Theme.of(context).accentColor,
//                    ),
//                    onPressed: () {},
//                  ),
//                  IconButton(
//                    icon: Icon(
//                      Icons.home,
//                      color: Theme.of(context).accentColor,
//                    ),
//                    onPressed: () {},
//                  ),
//                  IconButton(
//                    icon: Icon(
//                      Icons.home,
//                      color: Theme.of(context).accentColor,
//                    ),
//                    onPressed: () {},
//                  ),
//                ],
//              ),
//            ),
//            Align(
//              alignment: Alignment.bottomCenter,
//              child: GradientBottomNavigationBar(
//                backgroundColorStart: Colors.purple.withOpacity(0.1),
//                backgroundColorEnd: Colors.deepOrange.withOpacity(0.1),
//                items: <BottomNavigationBarItem>[
//                  BottomNavigationBarItem(
//                      icon: Icon(Icons.home), title: Text('Home')),
//                  BottomNavigationBarItem(
//                      icon: Icon(Icons.business), title: Text('Business')),
//                  BottomNavigationBarItem(
//                      icon: Icon(Icons.school), title: Text('School')),
//                ],
//                currentIndex: _selectedIndex,
//                onTap: (index) {},
//              ),
//            ),
//            Align(
//              alignment: Alignment.bottomCenter,
//              child: ClipRect(
//                child: BackdropFilter(
//                  filter: ImageFilter.blur(
//                    sigmaY: 5.0,
//                    sigmaX: 5.0,
//                  ),
//                  child: Container(
//                    color: Colors.blue,
//                    height: 56.0,
//                    child: Align(
//                      alignment: Alignment.topCenter,
////                      child: CubertoBottomBar(
////                        inactiveIconColor: Colors.blue,
////                        tabStyle: CubertoTabStyle.STYLE_FADED_BACKGROUND, // By default its CubertoTabStyle.STYLE_NORMAL
////                        selectedTab: _selectedIndex, // By default its 0, Current page which is fetched when a tab is clickd, should be set here so as the change the tabs, and the same can be done if willing to programmatically change the tab.
////                        tabs: [
////                          TabData(
////                            iconData: Icons.home,
////                            title: "Home",
////                            tabColor: Colors.deepPurple,
////                          ),
////                          TabData(
////                            iconData: Icons.search,
////                            title: "Search",
////                            tabColor: Colors.pink,
////                          ),
////                          TabData(
////                              iconData: Icons.access_alarm,
////                              title: "Alarm",
////                              tabColor: Colors.amber),
////                          TabData(
////                              iconData: Icons.settings,
////                              title: "Settings",
////                              tabColor: Colors.teal),
////                        ],
////                        onTabChangedListener: (position, title, color) {
////                          setState(() {
////                            _selectedIndex = position;
//////                    currentTitle = title;
//////                    currentColor = color;
////                          });
////                        },
////                      ),
//                    ),
//                  ),
//                ),
//              ),
//            ),
          ],
        ),
      ),
    );
  }
}
