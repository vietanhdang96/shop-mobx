import 'package:flutter/material.dart';
import 'package:state_management_example/model/entity/product.dart';
import 'package:state_management_example/shared/utils/app_variables.dart';
import 'package:state_management_example/util/style/app_colors.dart';
import 'package:state_management_example/util/widget/cart_app_bar.dart';
import 'package:state_management_example/util/widget/product_card.dart';

typedef GetProductListCallback = List<Product> Function();

class ProductListPageArgumentContainer {
  final Category category;
  final GetProductListCallback getProductListCallback;

  ProductListPageArgumentContainer(this.category, this.getProductListCallback);
}

class ProductListPage extends StatefulWidget {
  final ProductListPageArgumentContainer productListPageArgumentContainer;

  ProductListPage({
    @required this.productListPageArgumentContainer,
  });

  @override
  _ProductListPageState createState() => _ProductListPageState();
}

class _ProductListPageState extends State<ProductListPage> {
  @override
  Widget build(BuildContext context) {
    double statusbar = MediaQuery.of(context).padding.top;
    final productList =
        widget.productListPageArgumentContainer.getProductListCallback();
    return Scaffold(
      body: Container(
        color: AppColors.appGray1,
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 56 + statusbar),
              child: GridView.count(
                childAspectRatio: (1 / 1.5),
                padding: const EdgeInsets.all(0.0),
                crossAxisCount: 2,
                children: List.generate(productList.length, (index) {
                  final product = productList[index];
                  return ProductCard(product: product);
                }),
              ),
            ),
            CartAppBar(
              inHomePage: false,
              title: AppVariables.getCategoryString(
                  widget.productListPageArgumentContainer.category),
            ),
          ],
        ),
      ),
    );
  }
}
