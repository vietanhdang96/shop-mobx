import 'package:flutter/material.dart';
import 'package:state_management_example/shared/utils/app_variables.dart';
import 'package:state_management_example/util/style/app_colors.dart';
import 'package:state_management_example/util/style/app_fonts.dart';

class CategoryCard extends StatelessWidget {
  CategoryCard({
    this.title,
    this.text,
    this.color,
    this.iconData,
    this.category,
  });

  final String title;
  final String text;
  final Color color;
  final IconData iconData;
  final Category category;

  void navigateToProducts({BuildContext context, Category category}) {
//    Navigator.of(context).push<ProductListPage>(
//      MaterialPageRoute(
//        builder: (BuildContext context) => ProductListPage(category: category),
//      ),
//    );
//    Router.navigator.pushNamed(Router.productPage, arguments: Category.Games);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        width: double.infinity,
        height: 180,
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            title,
                            style: AppFonts.categoryCardTitle(),
                          ),
                          const SizedBox(height: 5),
                          Text(text, style: AppFonts.categoryCardTtext()),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(25.0, 15.0, 25.0, 15.0),
                      child: Ink(
                        decoration: BoxDecoration(
                          color: color,
                          borderRadius: BorderRadius.circular(15.0),
                          boxShadow: [
                            BoxShadow(
                              color: color,
                              blurRadius: 10,
                              spreadRadius: 3,
                            ),
                          ],
                        ),
                        child: InkWell(
                          borderRadius: BorderRadius.circular(15.0),
                          onTap: () {
                            navigateToProducts(
                                context: context, category: category);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.apps,
                                color: AppColors.appWhite,
                              ),
                              SizedBox(
                                width: 3.0,
                              ),
                              Text('Browse $title',
                                  style: AppFonts.categoryCardBtn()),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(3),
                    bottomRight: Radius.circular(3),
                    topLeft: Radius.circular(35),
                    bottomLeft: Radius.circular(35),
                  ),
                ),
                child: Icon(
                  iconData,
                  size: 75,
                  color: AppColors.appWhite,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
