import 'package:flutter/material.dart';
import 'package:state_management_example/util/style/app_colors.dart';
import 'package:state_management_example/util/style/app_fonts.dart';

class HorizontalListViewHeader extends StatelessWidget {
  final String listViewHeaderTitle;
  final GestureTapCallback onTapLoadMore;

  HorizontalListViewHeader({
    @required this.listViewHeaderTitle,
    @required this.onTapLoadMore,
  });

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return SizedBox(
      height: screenWidth / 7.5,
      child: Material(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: InkWell(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      listViewHeaderTitle,
                      style: AppFonts.categoryCardTitle(),
                    ),
                  ),
                ),
                onTap: onTapLoadMore,
              ),
            ),
            AspectRatio(
              aspectRatio: 1 / 1,
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Material(
                      elevation: 5.0,
                      clipBehavior: Clip.antiAlias,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(screenWidth),
                      ),
                      child: ConstrainedBox(
                        constraints: BoxConstraints.expand(),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment(-1.0, -1.0),
                              end: Alignment(1.0, 1.0),
                              colors: [
                                AppColors.appBlue1,
                                AppColors.appGreen,
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: IconButton(
                      icon: Icon(
                        Icons.keyboard_arrow_right,
                        color: Colors.white,
                      ),
                      onPressed: null,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Material(
                      color: Colors.transparent,
                      elevation: 0.0,
                      clipBehavior: Clip.antiAlias,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(screenWidth),
                      ),
                      child: Ink(
                        padding: EdgeInsets.all(0.0),
                        child: InkWell(
                          onTap: onTapLoadMore,
                        ),
                      ),
                    ),
                  ),
                ],
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Material(
//                    elevation: 5.0,
//                    clipBehavior: Clip.antiAlias,
//                    shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.circular(screenWidth),
//                    ),
//                    child: Ink(
//                      padding: EdgeInsets.all(0.0),
//                      decoration: BoxDecoration(
//                        gradient: LinearGradient(
//                          begin: Alignment(-1.0, -1.0),
//                          end: Alignment(1.0, 1.0),
//                          colors: [
//                            AppColors.appBlue1,
//                            AppColors.appGreen,
//                          ],
//                        ),
//                      ),
//                      child: InkWell(
//                        onTap: onTapLoadMore,
//                        child: Center(
//                          child: IconButton(
//                            icon: Icon(
//                              Icons.keyboard_arrow_right,
//                              color: Colors.white,
//                            ),
//                            onPressed: null,
//                          ),
//                        ),
//                      ),
//                    ),
//                  ),
//                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
