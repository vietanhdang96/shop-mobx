import 'package:flutter/material.dart';
import 'package:state_management_example/model/entity/product.dart';

import 'horizontal_listview.dart';
import 'horizontal_listview_header.dart';

class HorizontalListViewContainer extends StatelessWidget {
  final List<Product> productList;
  final String listViewHeaderTitle;
  final GestureTapCallback onTapLoadMore;

  HorizontalListViewContainer({
    @required this.productList,
    @required this.listViewHeaderTitle,
    @required this.onTapLoadMore,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Divider(
          color: Colors.black.withOpacity(0.2),
          thickness: 1.0,
          height: 1.0,
        ),
        HorizontalListViewHeader(
          listViewHeaderTitle: listViewHeaderTitle,
          onTapLoadMore: onTapLoadMore,
        ),
        HorizontalListView(
          productList: productList,
        ),
      ],
    );
  }
}
