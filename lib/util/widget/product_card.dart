import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management_example/model/entity/product.dart';
import 'package:state_management_example/store/other/cart/cart_store.dart';
import 'package:state_management_example/util/style/app_colors.dart';
import 'package:state_management_example/util/style/app_fonts.dart';

class ProductCard extends StatelessWidget {
  ProductCard({@required this.product});

  final Product product;

  void _addToCartOnClick(BuildContext context) {
    final cartStore = Provider.of<CartStore>(context, listen: false);
    cartStore.addToCart(product);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0)
      ),
      elevation: 5,
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(product.imageURL),
                ),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(3),
                  topLeft: Radius.circular(3),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: SizedBox.expand(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 5, 8, 0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            product.name,
                            style: AppFonts.productCardTitle(),
                            textAlign: TextAlign.start,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                          Text(
                            product.description,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: AppFonts.productCardTDescription(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 2),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 4,
                          child: Text('\$' + product.price.toString(),
                              style: AppFonts.productCardPrice()),
                        ),
                        Expanded(
                          flex: 6,
                          child: FractionallySizedBox(
                            heightFactor: 0.7,
                            child: RaisedButton(
                              onPressed: () => _addToCartOnClick(context),
                              padding: const EdgeInsets.all(0.0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              child: Ink(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  gradient: LinearGradient(
                                      begin: Alignment(-1.0, -1.0),
                                      end: Alignment(1.0, 1.0),
                                      colors: [
                                        AppColors.appBlue1,
                                        AppColors.appGreen,
                                      ]),
                                ),
                                child: Center(
                                  child: Text('Add to cart',
                                      style: AppFonts.productCardBtn()),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
