import 'package:flutter/material.dart';
import 'package:state_management_example/model/entity/product.dart';

import 'horizontal_listview_item.dart';

class HorizontalListView extends StatelessWidget {
  final List<Product> productList;

  HorizontalListView({@required this.productList});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return SizedBox(
        height: screenWidth / 2,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: productList.length,
          itemBuilder: (context, index) {
            return HorizontalListViewItem(
              product: productList[index],
              onTap: () => print(productList[index]),
            );
          },
        ));
  }
}
