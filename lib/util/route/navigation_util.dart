import 'package:state_management_example/page/product/product_list_page.dart';
import 'package:state_management_example/shared/utils/app_variables.dart';
import 'package:state_management_example/util/route/router.gr.dart';

class NavigationUtil {
  static Future navigateToProductListPage(
      Category category, GetProductListCallback getProductListCallback) {
    return Router.navigator.pushNamed(
      Router.productListPageRoute,
      arguments: ProductListPageArgumentContainer(
        category,
        getProductListCallback,
      ),
    );
  }
}
