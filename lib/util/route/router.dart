import 'package:auto_route/auto_route_annotations.dart';
import 'package:auto_route/transitions_builders.dart';
import 'package:state_management_example/main.dart';

@autoRouter
class $Router {
  @initial
  HomePageRoute homePageRoute;

  @CustomRoute(
    transitionsBuilder: TransitionsBuilders.slideLeftWithFade,
    durationInMilliseconds: 300,
  )
  ProductListPageRoute productListPageRoute;
}
