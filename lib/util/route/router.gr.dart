// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/router_utils.dart';
import 'package:state_management_example/main.dart';
import 'package:auto_route/transitions_builders.dart';
import 'package:state_management_example/page/product/product_list_page.dart';

class Router {
  static const homePageRoute = '/';
  static const productListPageRoute = '/productListPageRoute';
  static GlobalKey<NavigatorState> get navigatorKey =>
      getNavigatorKey<Router>();
  static NavigatorState get navigator => navigatorKey.currentState;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Router.homePageRoute:
        return MaterialPageRoute(
          builder: (_) => HomePageRoute(),
          settings: settings,
        );
      case Router.productListPageRoute:
        if (hasInvalidArgs<ProductListPageArgumentContainer>(args)) {
          return misTypedArgsRoute<ProductListPageArgumentContainer>(args);
        }
        final typedArgs = args as ProductListPageArgumentContainer;
        return PageRouteBuilder(
          pageBuilder: (ctx, animation, secondaryAnimation) =>
              ProductListPageRoute(typedArgs),
          settings: settings,
          transitionsBuilder: TransitionsBuilders.slideLeftWithFade,
          transitionDuration: Duration(milliseconds: 300),
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}
