import 'package:flutter/widgets.dart';
import 'package:state_management_example/model/entity/product.dart';
import 'package:state_management_example/shared/utils/app_variables.dart';

class Food extends Product {
  Food(
    String id,
    String name,
    String description,
    double price,
    String imageURL, {
    @required Category category,
  }) : super(id, name, description, Category.Food, price, imageURL);
}
