import 'package:state_management_example/shared/utils/app_variables.dart';

class Product {
  Product(
    this.id,
    this.name,
    this.description,
    this.category,
    this.price,
    this.imageURL,
  );

  final String id;
  final String name;
  final String description;
  final Category category;
  final double price;
  final String imageURL;

  @override
  bool operator ==(dynamic o) => o is Product && o.name == name;

  @override
  int get hashCode => name.hashCode;
}
