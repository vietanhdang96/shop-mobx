import 'package:state_management_example/model/entity/food.dart';
import 'package:state_management_example/model/repo/food/food_remote_data_source.dart';

class FoodRepository {
  final FoodRemoteDataSource _foodRemoteDataSource = FoodRemoteDataSource();

  List<Food> getRemoteFoodList() => _foodRemoteDataSource.getRemoteFoodList();
}
