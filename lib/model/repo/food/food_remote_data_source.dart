import 'package:state_management_example/model/entity/food.dart';
import 'package:state_management_example/shared/utils/app_variables.dart';

class FoodRemoteDataSource {
  List<Food> getRemoteFoodList() {
    return [
      Food(
        'Food_01',
        'Bluebarries',
        'Delicious blueberries from the wild.',
        30.0,
        'assets/pictures/Blueberries.jpg',
        category: Category.Food,
      ),
      Food(
        'Food_02',
        'Watermelon',
        'Water and suger in a red solid form.',
        40.0,
        'assets/pictures/Watermelon.jpg',
        category: Category.Food,
      ),
      Food(
        'Food_03',
        'Bluebarries 2',
        'Delicious blueberries from the wild.',
        30.0,
        'assets/pictures/Blueberries.jpg',
        category: Category.Food,
      ),
      Food(
        'Food_04',
        'Watermelon 2',
        'Water and suger in a red solid form.',
        40.0,
        'assets/pictures/Watermelon.jpg',
        category: Category.Food,
      ),
      Food(
        'Food_05',
        'Watermelon 3',
        'Water and suger in a red solid form.',
        40.0,
        'assets/pictures/Watermelon.jpg',
        category: Category.Food,
      ),
      Food(
        'Food_06',
        'Bluebarries 3',
        'Delicious blueberries from the wild.',
        30.0,
        'assets/pictures/Blueberries.jpg',
        category: Category.Food,
      ),
    ];
  }
}
