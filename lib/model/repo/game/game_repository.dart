import 'package:state_management_example/model/entity/game.dart';
import 'package:state_management_example/model/repo/game/game_remote_data_source.dart';

class GameRepository {
  final GameRemoteDataSource _gameRemoteDataSource = GameRemoteDataSource();

  List<Game> getRemoteGameList() => _gameRemoteDataSource.getRemoteGameList();
}
