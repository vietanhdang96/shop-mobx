import 'package:state_management_example/model/entity/game.dart';
import 'package:state_management_example/shared/utils/app_variables.dart';

class GameRemoteDataSource {
  List<Game> getRemoteGameList() {
    return [
      Game(
        'Game_01',
        'Mario Game',
        'Play as the famous plummer in the real world.',
        70.0,
        'assets/pictures/Mario.jpg',
        category: Category.Games,
      ),
      Game(
        'Game_02',
        'Dart',
        'Simple dart game.',
        20.0,
        'assets/pictures/Dart.jpg',
        category: Category.Games,
      ),
      Game(
        'Game_03',
        'Candy Crush',
        'Don\'t you have a phone?',
        20.0,
        'assets/pictures/Candy_Crush.jpg',
        category: Category.Games,
      ),
      Game(
        'Game_04',
        'Mario Game 2',
        'Play as the famous plummer in the real world.',
        70.0,
        'assets/pictures/Mario.jpg',
        category: Category.Games,
      ),
      Game(
        'Game_05',
        'Dart 2',
        'Simple dart game.',
        20.0,
        'assets/pictures/Dart.jpg',
        category: Category.Games,
      ),
      Game(
        'Game_06',
        'Candy Crush 2',
        'Don\'t you have a phone?',
        20.0,
        'assets/pictures/Candy_Crush.jpg',
        category: Category.Games,
      ),
    ];
  }
}
