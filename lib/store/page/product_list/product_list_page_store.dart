import 'package:mobx/mobx.dart';
import 'package:state_management_example/model/entity/food.dart';
import 'package:state_management_example/model/entity/game.dart';
import 'package:state_management_example/model/entity/product.dart';
import 'package:state_management_example/model/repo/food/food_repository.dart';
import 'package:state_management_example/model/repo/game/game_repository.dart';
import 'package:state_management_example/store/other/cart/cart_store.dart';

part 'product_list_page_store.g.dart';

class ProductListPageStore extends _ProductListPageStore with _$ProductListPageStore {
  ProductListPageStore()
      : super();
}

abstract class _ProductListPageStore with Store {
//  CartStore cartStore;
//  GameRepository gameRepository;
//  FoodRepository foodRepository;

//  _HomePageStore(this.cartStore, this.gameRepository, this.foodRepository);

  Function _freightReactionDisposer;

//  List<Game> getRemoteGameList() => gameRepository.getRemoteGameList();
//
//  List<Food> getRemoteFoodList() => foodRepository.getRemoteFoodList();

//  _CartStore() {
//    _freightReactionDisposer = reaction(
//      (_) => _cartContent.length,
//      (int cartItemCount) {
//        if (cartItemCount >= 10 && _freight == 0) {
//          _freight = 20;
//        } else if (cartItemCount < 10 && _freight != 0) {
//          _freight = 0;
//        }
//      },
//    );
//  }

  @observable
  ObservableList<Product> _cartContent = ObservableList<Product>();

  ObservableList<Product> get cartContent => _cartContent;

  @observable
  double _freight = 0;

  double get freight => _freight;

  int getProductQuantity(Product product) {
    return _cartContent.where((p) => p == product).length;
  }

  double getProductValue(Product product) {
    return ObservableList.of(_cartContent.where((p) => p == product))
        .fold<double>(0, (totalValue, product) => totalValue + product.price);
  }

  @computed
  double get cartValue => ObservableList.of(_cartContent)
      .fold<double>(0, (totalValue, product) => totalValue + product.price);

  @computed
  List<Product> get uniqueProducts =>
      ObservableList.of(_cartContent).toSet().toList();

  @action
  void addToCart(Product product) {
    _cartContent.add(product);
  }

  @action
  void removeProduct(Product product) {
    _cartContent.remove(product);
  }

  @action
  void removeAllFromCart(Product product) {
    _cartContent.removeWhere((p) => product == p);
  }

  @action
  void changeQuantity(Product product, int quantity) {
    int difference = quantity - getProductQuantity(product);
    while (difference != 0) {
      if (difference > 0) {
        addToCart(product);
        difference--;
      } else if (difference < 0) {
        removeProduct(product);
        difference++;
      }
    }
  }

  @action
  void emptyCart() {
    _cartContent.clear();
  }

  void dispose() {
    _freightReactionDisposer();
  }
}
