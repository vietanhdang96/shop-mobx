import 'package:mobx/mobx.dart';

part 'product_detail_page_store.g.dart';

class ProductDetailPageStore extends _ProductDetailPageStore
    with _$ProductDetailPageStore {
  ProductDetailPageStore() : super();
}

abstract class _ProductDetailPageStore with Store {

  _ProductDetailPageStore(){
    print('_ProductDetailPageStore init');
  }
}
