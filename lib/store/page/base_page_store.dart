import 'package:mobx/mobx.dart';

abstract class BasePageStore {

  @observable
  bool _isVisiblePage = true;

  bool get isVisiblePage => _isVisiblePage;

  BasePageStore();

  @action
  setVisiblePage(bool isVisiblePage) {
    _isVisiblePage = isVisiblePage;
    print('$_isVisiblePage');
  }
}
