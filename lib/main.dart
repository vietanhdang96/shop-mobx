import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management_example/model/repo/food/food_repository.dart';
import 'package:state_management_example/model/repo/game/game_repository.dart';
import 'package:state_management_example/page/home/home_page.dart';
import 'package:state_management_example/page/product/product_list_page.dart';
import 'package:state_management_example/store/other/cart/cart_store.dart';
import 'package:state_management_example/store/page/home/home_page_store.dart';
import 'package:state_management_example/store/page/product_detail/product_detail_page_store.dart';
import 'package:state_management_example/util/route/router.gr.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
//    - provided store
//        CartStore

//    - provided page store
//        ProductDetailPageStore

//    - provided repo
//        GameRepository
//        FoodRepository

    return MultiProvider(
      providers: [
        Provider<CartStore>(
          create: (_) => CartStore(),
          dispose: (_, cart) => cart.dispose(),
        ),
        Provider<GameRepository>(
          create: (_) => GameRepository(),
        ),
        Provider<FoodRepository>(
          create: (_) => FoodRepository(),
        ),
        Provider<ProductDetailPageStore>(
          create: (_) => ProductDetailPageStore(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: Router.homePageRoute,
        onGenerateRoute: Router.onGenerateRoute,
        navigatorKey: Router.navigatorKey,
      ),
    );
  }
}

class HomePageRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProxyProvider3<CartStore, GameRepository, FoodRepository,
        HomePageStore>(
      update: (_, cartStore, gameRepository, foodRepository, __) {
        return HomePageStore(cartStore, gameRepository, foodRepository);
      },
      child: HomePage(),
    );
  }
}

class ProductListPageRoute extends StatelessWidget {
  final ProductListPageArgumentContainer productListPageArgumentContainer;

  ProductListPageRoute(this.productListPageArgumentContainer);

  @override
  Widget build(BuildContext context) {
    return ProxyProvider3<CartStore, GameRepository, FoodRepository,
        HomePageStore>(
      update: (_, cartStore, gameRepository, foodRepository, __) {
        return HomePageStore(cartStore, gameRepository, foodRepository);
      },
      child: ProductListPage(
        productListPageArgumentContainer: productListPageArgumentContainer,
      ),
    );
  }
}
